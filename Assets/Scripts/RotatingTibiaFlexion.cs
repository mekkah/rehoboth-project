﻿using UnityEngine;
using System.Collections;

public class RotatingTibiaFlexion : MonoBehaviour {

    private float rotationOnZAxis;

    private bool rotateUp;

    // Use this for initialization
    void Start()
    {
        rotateUp = true;
        rotationOnZAxis = 0.178f;
    }

    // Update is called once per frame
    void Update()
    {
        IsMoving();

        if (rotateUp)
        {
            transform.Rotate(0f, 0f, rotationOnZAxis);
            //Debug.Log(transform.rotation.eulerAngles.z);
        }
        else
        {
            transform.Rotate(0f, 0f, rotationOnZAxis);
        }
    }

    void IsMoving()
    {
        if (transform.rotation.eulerAngles.z < 1f)
        {
            rotateUp = true;
            rotationOnZAxis = 0.178f;
        }
        if (transform.rotation.eulerAngles.z > 13f)
        {
            rotateUp = false;
            rotationOnZAxis = -0.178f;
        }

    }
}
