﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class TranslatingFemurFlexion : MonoBehaviour {
    private float translationOnXAxis;

    private bool moveLeft;

    public Text liveAPTranslationStream;

    // Use this for initialization
    void Start()
    {
        moveLeft = true;
        translationOnXAxis = -0.010f;
    }

    // Update is called once per frame
    void Update()
    {
        IsMoving();

        if (moveLeft)
        {
            transform.Translate(translationOnXAxis, 0f, 0f);
            //Debug.Log(transform.position.x);
        }
        else
        {
            transform.Translate(translationOnXAxis, 0f, 0f);
        }

        //Shows the translation of the Femur moving on the X axis
        liveAPTranslationStream.text = transform.position.x.ToString();
    }

    void IsMoving()
    {
        if (transform.position.x < 66.598f)
        {
            moveLeft = true;
            translationOnXAxis = 0.00435f;
        }
        if (transform.position.x > 66.90f)
        {
            moveLeft = false;
            translationOnXAxis = -0.00435f;
        }

    }
}
