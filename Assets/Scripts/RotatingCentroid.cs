﻿using UnityEngine;
using System.Collections;

public class RotatingCentroid : MonoBehaviour {

    private float rotationOnXAxis;

    private bool rotateUp;

    // Use this for initialization
    void Start()
    {
        rotateUp = true;
        rotationOnXAxis = -0.312f;
    }

    // Update is called once per frame
    void Update()
    {
        IsMoving();

        if (rotateUp)
        {
            transform.Rotate(0f, rotationOnXAxis, 0f);
            //Debug.Log(transform.rotation.eulerAngles.z);
        }
        else
        {
            transform.Rotate(0f, rotationOnXAxis, 0f );
        }
    }

    void IsMoving()
    {
        if (transform.rotation.eulerAngles.x > 4f)
        {
            rotateUp = true;
            rotationOnXAxis = -0.312f;
        }
        if (transform.rotation.eulerAngles.x < 1f)
        {
            rotateUp = false;
            rotationOnXAxis = 0.312f;
        }

    }
}
