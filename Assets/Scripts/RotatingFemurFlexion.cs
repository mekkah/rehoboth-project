﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RotatingFemurFlexion: MonoBehaviour
{    
    private float rotationOnZAxis;
    private bool rotateUp;
    public Text liveAngleStream;

    // Use this for initialization
    void Start()
    {
        rotateUp = true;
        rotationOnZAxis = -0.278f;
    }

    // Update is called once per frame
    void Update()
    {
        IsMoving();

        if (rotateUp)
        {
            //Rotates the 3D model
            transform.Rotate(0f, 0f, rotationOnZAxis);

            //Debug.Log(transform.rotation.eulerAngles.z);
        }
        else
        {
            //Rotates the 3D model
            transform.Rotate(0f, 0f, rotationOnZAxis);
        }

        //Shows the Euler Angle to the UI
        liveAngleStream.text = (transform.rotation.eulerAngles.z -180).ToString();
    }

    void IsMoving()
    { 
        if (transform.rotation.eulerAngles.z < 256f)
        {
            rotateUp = true;
            rotationOnZAxis = 0.278f;
        }
        if (transform.rotation.eulerAngles.z > 280f)
        {
            rotateUp = false;
            rotationOnZAxis = -0.278f;
        }
       
    }
}
