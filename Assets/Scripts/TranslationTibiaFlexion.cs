﻿using UnityEngine;
using System.Collections;

public class TranslationTibiaFlexion : MonoBehaviour {
    private float translationOnXAxis;

    private bool moveLeft;

    // Use this for initialization
    void Start()
    {
        moveLeft = true;
        translationOnXAxis = -0.00335f;
    }

    // Update is called once per frame
    void Update()
    {
        IsMoving();

        if (moveLeft)
        {
            transform.Translate(translationOnXAxis, 0f, 0f);
            //Debug.Log(transform.position.x);
        }
        else
        {
            transform.Translate(translationOnXAxis, 0f, 0f);
        }
    }

    void IsMoving()
    {
        if (transform.position.x < 66.568f)
        {
            moveLeft = true;
            translationOnXAxis = 0.00335f;
        }
        if (transform.position.x > 66.797f)
        {
            moveLeft = false;
            translationOnXAxis = -0.00335f;
        }

    }
}
